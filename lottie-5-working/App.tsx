import React, { useEffect, useState } from "react";
import { StyleSheet, Text, View } from "react-native";
import { Asset } from "expo-asset";
import LottieView from "lottie-react-native";

export default function App() {
  /** Using a public lottie animation as an example */
  const remoteUrl =
    "https://lottie.host/57aae058-e066-483b-b16b-8851915729cc/JLtMKwvR4e.json";

  const [localUri, setLocalUri] = useState<string>();

  /** Download remote file to local and retrieve uri */
  useEffect(() => {
    Asset.fromURI(remoteUrl)
      .downloadAsync()
      .then((asset) => {
        if (asset.localUri) {
          setLocalUri(asset.localUri);
        }
      });
  }, []);

  console.log({ localUri });

  return (
    <View style={styles.container}>
      {/* Render local animation when localUri exists */}
      {!!localUri ? (
        <View style={{ flex: 1, margin: 20, borderWidth: 2 }}>
          <LottieView
            source={{ uri: localUri }}
            // If you change to use the remote url below, it will work.
            // source={{ uri: remoteUrl }}
            style={{ flex: 1 }}
            autoPlay
            onAnimationFailure={(error) => {
              console.log({ error });
            }}
            loop
          />
        </View>
      ) : (
        <Text>Loading...</Text>
      )}
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    width: "100%",
    height: "50%",
    backgroundColor: "#fff",
    flexDirection: "row",
  },
});
